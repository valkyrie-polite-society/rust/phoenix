use anyhow::Result;
use crate::environments::*;
use serde::{Serialize, Deserialize};
use shellexpand::tilde;

#[derive(Default, PartialEq, Debug, Serialize, Deserialize)]
pub struct PhoenixConfig {
  pub r#static: Option<Vec<Environment>>,
  pub aws: Option<AwsTopLevel>,
}

#[derive(Default, PartialEq, Debug, Serialize, Deserialize)]
pub struct AwsTopLevel {
  pub regions: Option<Vec<String>>,
  pub r#static: Option<Vec<Environment>>,
  pub eks: Option<Vec<EksEnvironmentsSpec>>,
  pub ec2: Option<Vec<EC2EnvironmentsSpec>>,
}

pub fn from_file(file: String) -> Result<PhoenixConfig> {
  let expanded = tilde(&file).to_string();
  let reader = std::fs::File::open(expanded)?;
  let config: PhoenixConfig = serde_yaml::from_reader(reader)?;
  Ok(config)
}

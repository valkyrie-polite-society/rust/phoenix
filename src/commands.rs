use anyhow::{anyhow, ensure, Result};
use crate::environments;
use crate::environments::Environment;
use crate::prompts;
use crate::opts::{Restore, Save};
use crate::config::PhoenixConfig;

async fn environment_or_prompt(config: &PhoenixConfig, cli_environment_name: Option<String>) -> Result<Environment> {
  let environments = environments::from_config(config).await?;

  ensure!(environments.len() > 0, "No environments found");

  match cli_environment_name {
    Some(name) => environments.iter().cloned()
        .find(|env| env.name == name)
        .ok_or(anyhow!("Environment {} not found", name)),
    None => Ok(prompts::environment(&environments))
  }
}

pub async fn save(save: Save, config: PhoenixConfig) -> Result<()> {
  let environment = environment_or_prompt(&config, save.environment).await?;

  println!("Saving snapshot from environment {} with endpoint {}...", environment.name, environment.endpoint_uri);

  Ok(())
}

pub async fn restore(restore: Restore, config: PhoenixConfig) -> Result<()> {
  let environment = environment_or_prompt(&config, restore.environment).await?;

  println!("Restoring snapshot to environment {} with endpoint {}...", environment.name, environment.endpoint_uri);

  Ok(())
}

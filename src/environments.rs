use anyhow::Result;
use crate::config::PhoenixConfig;
use rusoto_core::{Region, HttpClient};
use rusoto_credential::*;
use rusoto_ec2::*;
use rusoto_eks::*;
use serde::{Serialize, Deserialize};
use std::collections::HashSet;
use std::str::FromStr;

#[derive(Clone, Default, PartialEq, Debug, Serialize, Deserialize)]
pub struct Environment {
  pub name: String,
  pub endpoint_uri: String,
  pub snapshot_storage_uri: String,
}

#[derive(Clone, Default, PartialEq, Debug, Serialize, Deserialize)]
pub struct EksEnvironmentsSpec {
  pub profile: Option<String>,
  pub regions: Option<Vec<String>>,
  pub tag: String,
}

#[derive(Clone, Default, PartialEq, Debug, Serialize, Deserialize)]
pub struct EC2EnvironmentsSpec {
  pub profile: Option<String>,
  pub regions: Option<Vec<String>>,
  pub tag: String,
}

pub async fn from_config(config: &PhoenixConfig) -> Result<Vec<Environment>> {
  let mut environments: Vec<Environment> = Vec::new();

  println!("Fetching static environments...");
  if let Some(static_environments) = &config.r#static {
    for env in static_environments {
      environments.push(env.clone())
    }
  }

  if let Some(aws) = &config.aws {
    println!("Fetching environments in EKS clusters...");
    if let Some(specs) = &aws.eks {
      for spec in specs {
        let provider = chain_provider_with_profile(spec.profile.clone())?;
        let filter_tag = spec.tag.clone();
        let regions = get_aws_regions(&spec.regions, &aws.regions);

        for env in from_eks_clusters(provider, &regions, filter_tag).await? {
          environments.push(env)
        }
      }
    }
  
    println!("Fetching environments in EC2 instances...");
    if let Some(specs) = &aws.ec2 {
      for spec in specs {
        let provider = chain_provider_with_profile(spec.profile.clone())?;
        let filter_tag = spec.tag.clone();
        let regions = get_aws_regions(&spec.regions, &aws.regions);

        for env in from_ec2_instances(provider, &regions, filter_tag).await? {
          environments.push(env)
        }
      }
    }
  }

  Ok(environments)
}

fn chain_provider_with_profile(profile: Option<String>) -> Result<ChainProvider> {
  let mut profile_provider = ProfileProvider::new()?;

  if profile.is_some() {
    profile_provider.set_profile(profile.unwrap());
  }

  println!("Using chain credentials provider with profile: {}", profile_provider.profile());

  Ok(ChainProvider::with_profile_provider(profile_provider))
}

fn get_aws_regions(spec_regions: &Option<Vec<String>>, aws_regions: &Option<Vec<String>>) -> Vec<Region> {
  if let Some(regions) = spec_regions {
    return regions.iter().map(|s| Region::from_str(s).unwrap()).collect();
  }

  if let Some(regions) = aws_regions {
    return regions.iter().map(|s| Region::from_str(s).unwrap()).collect();
  }

  Vec::new()
}

async fn eks_list_all_clusters(provider: ChainProvider, regions: &Vec<Region>) -> Result<Vec<Cluster>> {
  let mut clusters: Vec<Cluster> = Vec::new();

  // TODO: Fix inefficent asynchronous programming
  for region in regions {
    let client = EksClient::new_with(HttpClient::new().unwrap(), provider.clone(), region.clone());
    for cluster in eks_list_clusters(&client).await? {
      let result = eks_describe_cluster(&client, cluster);
      clusters.push(result.await.unwrap());
    }
  }

  Ok(clusters)
}

async fn eks_list_clusters(client: &EksClient) -> Result<Vec<String>> {
  let response: ListClustersResponse = client.list_clusters(ListClustersRequest::default()).await?;

  match response.clusters {
    Some(clusters) => Ok(clusters),
    None => Ok(Vec::new())
  }
}

async fn eks_describe_cluster(client: &EksClient, cluster: String) -> Option<Cluster> {
  let resp = client.describe_cluster(DescribeClusterRequest{
    name: cluster
  });

  resp.await.ok()?.cluster
}

async fn from_eks_clusters(provider: ChainProvider, regions: &Vec<Region>, tag: String) -> Result<Vec<Environment>> {
  let mut environments: Vec<Environment> = Vec::new();
  
  for cluster in eks_list_all_clusters(provider, regions).await? {
    if let Some(tags) = cluster.tags {
      if tags.contains_key(&tag) {
        let name = cluster.name.unwrap();
        environments.push(Environment {
          name: name.clone(),
          endpoint_uri: format!("https://{}.example.com", name),
          snapshot_storage_uri: format!("s3://{}-backups/", name),
        })
      }
    }
  }

  Ok(environments)
}


async fn from_ec2_instances(provider: ChainProvider, regions: &Vec<Region>, tag: String) -> Result<Vec<Environment>> {
  let mut environments: Vec<Environment> = Vec::new();
  let mut distinct_tags = HashSet::new();

  // TODO: Fix inefficent asynchronous programming
  for region in regions {
    let client = Ec2Client::new_with(HttpClient::new().unwrap(), provider.clone(), region.clone());
    let result = client.describe_instances(DescribeInstancesRequest::default()).await?;
    
    if let Some(reservations) = result.reservations {
      for reservation in reservations {
        if let Some(instances) = reservation.instances {
          for instance in instances {
            if let Some(tags) = instance.tags {
              for each in tags {
                if each.key.unwrap_or(String::default()) == tag {
                  distinct_tags.insert(each.value.unwrap());
                  ()
                }
              }
            }
          }
        }
      }
    }
  }

  for tag in distinct_tags {
    environments.push(Environment{
      name: tag,
      endpoint_uri: String::new(),
      snapshot_storage_uri: String::new()
    })
  }

  Ok(environments)
}

pub use crate::clap::Clap;

#[derive(Clap)]
#[clap(version = "0.9", author = "Adrienne Cohea <adriennecohea@gmail.comm>")]
pub struct Opts {
    /// Configuration file path
    #[clap(short, long, default_value = "~/.config/phoenix/default.yaml")]
    pub config_file: String,


    #[clap(subcommand)]
    pub subcmd: SubCommand,
}

#[derive(Clap)]
pub enum SubCommand {
    #[clap()]
    Save(Save),

    #[clap()]
    Restore(Restore),
}

/// Save a raft snapshot from your environment and send it to external storage
#[derive(Clap)]
pub struct Save {
  /// Environment to snapshot and send to storage
  #[clap(short, long)]
  pub environment: Option<String>,
}

/// Restore a raft snapshot from secure storage to your environment
#[derive(Clap)]
pub struct Restore {
  /// Environment to restore from snapshot storage
  #[clap(short, long)]
  pub environment: Option<String>,
}

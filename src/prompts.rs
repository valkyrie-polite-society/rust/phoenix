use crate::environments::Environment;
use dialoguer::{theme::ColorfulTheme, Select};

pub fn environment(environments: &Vec<Environment>) -> Environment {
  let names: Vec<String> = environments.iter().map(|e| e.name.clone()).collect();

  let choices = names.as_slice();

  let selection = Select::with_theme(&ColorfulTheme::default())
      .with_prompt("Select environment")
      .default(0)
      .items(&choices[..])
      .interact()
      .unwrap();
    
  environments[selection].clone()
}

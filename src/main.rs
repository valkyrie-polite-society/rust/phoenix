mod environments;
mod prompts;
mod opts;
mod commands;
mod config;

extern crate clap;
extern crate serde;

use anyhow::Result;
use opts::*;
use config::*;

#[tokio::main]
pub async fn main() -> Result<()> {
  let opts = Opts::parse();
  let config: PhoenixConfig = config::from_file(opts.config_file)?;

  match opts.subcmd {
    SubCommand::Save(save) => commands::save(save, config).await,
    SubCommand::Restore(restore) => commands::restore(restore, config).await,
  }
}
